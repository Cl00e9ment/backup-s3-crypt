FROM alpine:3

RUN apk add aws-cli bash coreutils gnupg xz
WORKDIR /backup/
ENV PATH="/backup:${PATH}"
COPY scripts/* ./
RUN chmod 750 cron.sh backup list fetch \
	&& mkdir /to-backup/

CMD ./cron.sh
