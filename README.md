# Backup S3 Crypt

*[Open Source](https://gitlab.com/Cl00e9ment/backup-s3-crypt) image based on [Alpine Linux](https://hub.docker.com/_/alpine).*

- Makes **periodic** backups to S3.
- **Compresses** and **encrypts** backups before sending them to S3.
- **Aborts** the backup creation if the folder was **not modified**.
- Can **delete old backups** from S3 to keep only the most recent ones.
- Can **quickly fetch a backup** from S3 then decrypt it and decompress it on the fly.

## Start the container

### Example

```bash
docker run -it --rm --name backup-s3-crypt \
	-v /home/user/my_files_1/:/to-backup/my_files_1/
	-v /home/user/my_files_2/:/to-backup/my_files_2/
	-e AWS_ACCESS_KEY_ID=MY_AWS_KEY_ID \
	-e AWS_SECRET_ACCESS_KEY=my_super_secret_aws_access_key \
	-e AWS_DEFAULT_REGION=eu-west-3 \
	-e AWS_BUCKET_NAME=my-bucket-name \
	-e ENCRYPTION_PASSPHRASE=My-5uP3R_Str0NG-_p@55PhrAse \
	-e MAX_BACKUPS=7 \
	-e CRON_TIME='0 5 * * *' \
	cl00e9ment/backup-s3-crypt
```

### Explanation of environment variables

| Variable              | Required | Description                                                                                                                               |
| :-------------------- | :------: | :---------------------------------------------------------------------------------------------------------------------------------------- |
| AWS_ACCESS_KEY_ID     |   yes    | The ID of the AWS access key used to upload backups to S3.                                                                                |
| AWS_SECRET_ACCESS_KEY |   yes    | The AWS access key used to upload backups to S3.                                                                                          |
| AWS_DEFAULT_REGION    |   yes    | The AWS region where the bucket lives.                                                                                                    |
| AWS_BUCKET_NAME       |   yes    | The name of the AWS bucket where to upload the backups.                                                                                   |
| ENCRYPTION_PASSPHRASE |   yes    | The passphrase used to encrypt the backups before sending them to S3.                                                                     |
| MAX_BACKUPS           |    no    | The max number of backups of a specific folder to keep on S3. If set, old backups will be deleted. If not set, no backup will be deleted. |
| CRON_TIME             |   yes    | A [cron schedule expression](https://crontab.guru/#0_0_*_*_*) to indicate when the backups must be made (in UTC time).                    |

### Important considerations

**!!! WARNING !!!**  
**Keep a copy of the ENCRYPTION_PASSPHRASE outside of your server, because if a problem occurs on your server, you will not be able to recover your backups.**

**!!! WARNING !!!**  
**Do not enable bucket versioning, because Backup S3 Crypt handle versioning by itself.**

## CLI

Commands that can be run when the container is running.

### Manually start a backup

Replace `[FOLDERS]...` by a list of space separated folders to backup or leave it empty to backup all folders mounted to `/to-backup/`.

```bash
docker exec backup-s3-crypt backup [FOLDERS]...
```

### List all backups

```bash
docker exec backup-s3-crypt list
```

### List backups of specific folder

```bash
docker exec backup-s3-crypt list my_files_1
```

### Fetch a backup

You can use the fetch command to get a backup from AWS.
The script will download, decrypt and decompress the file for you (but you still need to unarchive it):

```
docker exec backup-s3-crypt fetch 0a309941293fb9db23cc3c2293c542e83622d313-20210227025458-8c34f5b1ef4c5feacb672f75975f26bc655bb842.tar.xz.gpg
docker cp backup-s3-crypt:/tmp/fetched-backup.tar .
tar -xf fetched-backup.tar
```
