#!/bin/bash

function check_env {
	requiredVars='AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_DEFAULT_REGION AWS_BUCKET_NAME ENCRYPTION_PASSPHRASE CRON_TIME'

	requiredVarsMissing='0'
	for var in $requiredVars; do
		if [ -z "${!var}" ]; then
			echo "${var} is not set" >&2
			requiredVarsMissing='1'
		fi
	done

	if [ "$requiredVarsMissing" == '1' ]; then
		exit 1
	fi
}

check_env
echo "${CRON_TIME} backup" | crontab -
crond -f
